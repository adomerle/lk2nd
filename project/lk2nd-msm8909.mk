# SPDX-License-Identifier: BSD-3-Clause
TARGET := msm8909
include lk2nd/project/lk2nd.mk
ENABLE_PARTIAL_GOODS_SUPPORT = 0
DEFINES := $(filter-out ENABLE_PARTIAL_GOODS_SUPPORT=1, $(DEFINES))
